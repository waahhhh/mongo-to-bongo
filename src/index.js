$.wait = function (callback, ms) {
    return window.setTimeout(callback, ms);
}

/**
 * @param {{note: string, duration: number}[]} melody
 */
$.playMelody = function (melody) {
    const ANIMATION_DURATION = 50;
    var previousDuration = 0;

    melody.forEach((tone) => {
        const note = tone.note;
        const duration = tone.duration;

        $.wait(() => {
            document.dispatchEvent(new KeyboardEvent('keydown', {'key': note}));
        }, previousDuration);

        // Execute another event to release the key.
        // With an additional duration so that the animation is displayed correctly.
        $.wait(() => {
            document.dispatchEvent(new KeyboardEvent('keyup', {'key': note}));
        }, previousDuration + ANIMATION_DURATION);

        // add to the current duration so that the next events build on it.
        previousDuration += (duration + ANIMATION_DURATION);
    });
}
