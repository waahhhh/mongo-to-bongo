# mongo to bongo
This is a script to play melodies on the website https://bongo.cat.  
For all mongos, who are also too incapable to hit the rhythm and the notes in the right moment.  

## Usage
Execute the code from the [src/index.js](src/index.js).  
Then call the `playMelody` method with the appropriate melodie.  

For example "Happy birthday":  
```text
11 3 1 6 5
11 3 1 8 6
```
Source: https://github.com/Externalizable/bongo.cat/discussions/141  

```javascript
$.playMelody([
    {"note": "1", "duration": 200},
    {"note": "1", "duration": 200},
    {"note": "3", "duration": 400},
    {"note": "1", "duration": 400},
    {"note": "6", "duration": 400},
    {"note": "5", "duration": 400},

    {"note": "1", "duration": 200},
    {"note": "1", "duration": 200},
    {"note": "3", "duration": 400},
    {"note": "1", "duration": 400},
    {"note": "8", "duration": 400},
    {"note": "5", "duration": 400}
]);
```

More melodies can be add/found at: [melodies](melodies)  

## API
### playMelody
This method expects an array of objects.  
The objects should have the 2 attributes `note` and `duration`.  
Each object represents one keystroke.  

The attibute `note` contains the key which should be pressed.  

The attibute `duration` defines the time in milliseconds until the next key should be pressed.  

We have also defined a constant `ANIMATION_DURATION`, which waits 50 milliseconds between each keystroke to release the keypress.  

Example:  
```javascript
$.playMelody([
    {"note": "1", "duration": 200}
]);
```

## Contribution
Contributions are very welcome.  
Preferably for translations of other melodies.  
And of course also for [bongo.cat: submit your own!](https://github.com/Externalizable/bongo.cat/discussions/new)  

## License
This project is open-sourced software licensed under the MIT license - see the [LICENSE](LICENSE) file for details.  
