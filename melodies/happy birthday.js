/** @see {@link https://github.com/Externalizable/bongo.cat/discussions/141} */
$.playMelody([
  {"note": "1", "duration": 200},
  {"note": "1", "duration": 200},
  {"note": "3", "duration": 400},
  {"note": "1", "duration": 400},
  {"note": "6", "duration": 400},
  {"note": "5", "duration": 400},

  {"note": "1", "duration": 200},
  {"note": "1", "duration": 200},
  {"note": "3", "duration": 400},
  {"note": "1", "duration": 400},
  {"note": "8", "duration": 400},
  {"note": "5", "duration": 400}
]);
