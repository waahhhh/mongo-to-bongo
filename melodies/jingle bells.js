/** @see {@link https://github.com/Externalizable/bongo.cat/discussions/34} */
$.playMelody([
  {"note": "7", "duration": 200},
  {"note": "7", "duration": 200},
  {"note": "7", "duration": 400},

  {"note": "7", "duration": 200},
  {"note": "7", "duration": 200},
  {"note": "7", "duration": 200},
  {"note": "7", "duration": 400},

  {"note": "0", "duration": 200},
  {"note": "3", "duration": 200},
  {"note": "5", "duration": 200},
  {"note": "7", "duration": 200},

  {"note": "8", "duration": 200},
  {"note": "8", "duration": 200},
  {"note": "8", "duration": 200},
  {"note": "8", "duration": 200},
  {"note": "8", "duration": 200},
  {"note": "7", "duration": 200},
  {"note": "7", "duration": 400},

  {"note": "7", "duration": 200},
  {"note": "7", "duration": 200},
  {"note": "7", "duration": 200},
  {"note": "5", "duration": 200},
  {"note": "5", "duration": 200},
  {"note": "7", "duration": 200},
  {"note": "5", "duration": 400},

  {"note": "0", "duration": 400}
]);

