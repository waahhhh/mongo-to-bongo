$.playMelody([
    {"note": "2", "duration": 300},
    {"note": "3", "duration": 300},
    {"note": "4", "duration": 300},
    {"note": "2", "duration": 300},

    {"note": "2", "duration": 300},
    {"note": "3", "duration": 300},
    {"note": "4", "duration": 300},
    {"note": "2", "duration": 300},

    {"note": "4", "duration": 300},
    {"note": "5", "duration": 300},
    {"note": "6", "duration": 600},

    {"note": "4", "duration": 300},
    {"note": "5", "duration": 300},
    {"note": "6", "duration": 600},

    {"note": "6", "duration": 100},
    {"note": "7", "duration": 100},
    {"note": "6", "duration": 100},
    {"note": "5", "duration": 300},
    {"note": "4", "duration": 300},
    {"note": "2", "duration": 300},

    {"note": "6", "duration": 100},
    {"note": "7", "duration": 100},
    {"note": "6", "duration": 100},
    {"note": "5", "duration": 300},
    {"note": "4", "duration": 300},
    {"note": "2", "duration": 300},

    {"note": "2", "duration": 300},
    {"note": "1", "duration": 300},
    {"note": "2", "duration": 600},

    {"note": "2", "duration": 300},
    {"note": "1", "duration": 300},
    {"note": "2", "duration": 600}
]);
